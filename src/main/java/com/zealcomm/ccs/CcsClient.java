package com.zealcomm.ccs;

import android.content.Context;
import android.util.Log;

import com.zealcomm.base.MessageData;
import com.zealcomm.base.TLSSocketFactory;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.Arrays;

import javax.net.ssl.X509TrustManager;

import io.socket.client.Ack;
import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.OkHttpClient;

public class CcsClient {

    private final String TAG = "CcsClient";
    /**
     * for agent
     */
    private final String LOGIN = "join-session";
    /**
     * for customer
     */
    private final String REQUEST_SESSION = "request-session";

    public final String STATUS_OK = "ok";
    private final String SESSION_MESSAGE = "session-message";
    private final String QUIT = "quit-session";
    private final String DROP = "drop";
    private static CcsClient mCcsClient;
    private String mToken;
    private Socket mIvcsSocket;
    private boolean isConnected;
    private Context mContext;
    private String mCcsUrl;
    private IO.Options options;
    private CcsEventMessage mCcsEventMessage;

    private CcsClient() {

    }

    public static CcsClient getInstance() {

        if (mCcsClient == null) {
            mCcsClient = new CcsClient();
        }
        return mCcsClient;
    }

    public void init(Context context, CcsEventMessage ccsEventMessage) {
        mContext = context;
        mCcsEventMessage = ccsEventMessage;
        options = new IO.Options();
        options.forceNew = true;
        options.reconnection = true;
        options.secure = false;
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        if (TLSSocketFactory.sslContext != null) {
            clientBuilder.sslSocketFactory(TLSSocketFactory.delegate, TLSSocketFactory.DEFAULT_TRUST_MANAGERS);
        }
        if (TLSSocketFactory.mHostnameVerifier != null) {
            clientBuilder.hostnameVerifier(TLSSocketFactory.mHostnameVerifier);
        }
        OkHttpClient httpClient = clientBuilder.build();
        options.callFactory = httpClient;
        options.webSocketFactory = httpClient;
    }

    public void connect(String token, String ccsUrl, String query) {
//        ccsUrl = ccsUrl + "/socket.io" ;
        Log.i(TAG, "ccs =" + ccsUrl);
        Log.i(TAG, "query = " + query);
        mCcsUrl = ccsUrl;
        mToken = token;
        options.query = "servicePath=_ivcs_ccs&token=" + mToken + (query != null ? ("&" + query) : "");
        try {
            mIvcsSocket = IO.socket(mCcsUrl, options);
        } catch (URISyntaxException e) {
            throw new RuntimeException(e);
        }
        mIvcsSocket.on(Socket.EVENT_CONNECTING, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG , "connecting:" + Arrays.asList(args)) ;
            }
        }) ;
        mIvcsSocket.on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                Log.i(TAG , "EVENT_ERROR:" + Arrays.asList(args)) ;
            }
        }) ;
        mIvcsSocket.on(Socket.EVENT_CONNECT, onConnect);
        mIvcsSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mIvcsSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        // io.socket:socket.io-client 由 1.0.0 升级到 2.0.0 后将 Socket.EVENT_CONNECT_TIMEOUT 合并到 Socket.EVENT_CONNECT_ERROR 了
        mIvcsSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mIvcsSocket.on(DROP, drop);
        mIvcsSocket.on(SESSION_MESSAGE, onSessionMessage);
        mIvcsSocket.connect();

    }

    public void ccsLogin(JSONObject loginData) {
        mIvcsSocket.emit(LOGIN, loginData, new Ack() {
            @Override
            public void call(Object... args) {
                if (isConnected) {
                    String res = (String) args[0];
                    Log.i(TAG, "login ccs ack =" + args[1]);
                    if (res.equals(STATUS_OK)) {
                        JSONObject data = (JSONObject) args[1];
                        MessageData messageData = new MessageData();
                        messageData.setData(data);
                        messageData.setType(CcsMessage.CCS_LOGIN_SUCCESS);
                        mCcsEventMessage.onCcsEvent(messageData);
                    } else {
                        String resp = (String) args[1];
                        Log.e(TAG, "ccsLogin error " + resp);
                        MessageData messageData = new MessageData();
                        messageData.setType(CcsMessage.CCS_LOGIN_ERROR);
                        mCcsEventMessage.onCcsEvent(messageData);
                    }
                }
            }
        });
    }

    public void ccsSession(JSONObject sessionData) {

        mIvcsSocket.emit(REQUEST_SESSION, sessionData, new Ack() {
            @Override
            public void call(Object... args) {
                if (isConnected) {
                    String res = (String) args[0];
                    Log.e(TAG, "ccsSession ====  " + Arrays.asList(args));
                    if (res.equals(STATUS_OK)) {
                        JSONObject data = (JSONObject) args[1];
                        MessageData messageData = new MessageData();
                        messageData.setData(data);
                        messageData.setType(CcsMessage.CCS_LOGIN_SUCCESS);
                        mCcsEventMessage.onCcsEvent(messageData);
                    } else {
                        String resp = (String) args[1];
                        Log.e(TAG, "ccsSession error " + resp);
                        MessageData messageData = new MessageData();
                        messageData.setType(CcsMessage.CCS_LOGIN_ERROR);
                        mCcsEventMessage.onCcsEvent(messageData);
                    }
                }
            }
        });
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.i(TAG, "onConnect = " + args);
            if (!isConnected) {
                isConnected = true;
                MessageData messageData = new MessageData();
                messageData.setType(CcsMessage.CCS_CONNECT_SUCCESS);
                mCcsEventMessage.onCcsEvent(messageData);
            }
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.i(TAG, "disconnected");
            isConnected = false;
            MessageData messageData = new MessageData();
            messageData.setType(CcsMessage.CCS_DISCONNECT);
            mCcsEventMessage.onCcsEvent(messageData);
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e(TAG, "Error connecting" + Arrays.asList(args));
            MessageData messageData = new MessageData();
            messageData.setType(CcsMessage.CCS_CONNECT_ERROR);
            mCcsEventMessage.onCcsEvent(messageData);
        }
    };

    private Emitter.Listener drop = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            Log.e(TAG, "drop");
            MessageData messageData = new MessageData();
            messageData.setType(CcsMessage.CCS_DROP);
            mCcsEventMessage.onCcsEvent(messageData);
        }
    };

    private Emitter.Listener onSessionMessage = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            if (isConnected) {
                //start to parse peer status message
                try {
                    JSONObject message = (JSONObject) args[0];
                    Log.d(TAG, "onSessionMessage : " + message.toString());
                    String type = message.getString("type");
                    MessageData messageData = new MessageData();
                    messageData.setData(message);
                    if (CcsMessage.CCS_FORM.equals(type)
                            && message.has("data")
                            && message.getJSONObject("data").has("collect")
                            && message.getJSONObject("data").getInt("collect") == 1){
                        messageData.setType(CcsMessage.CCS_COLLECT_INFO);
                    }else {
                        messageData.setType(type);
                    }
                    mCcsEventMessage.onCcsEvent(messageData);
                } catch (Exception e) {
                    Log.e(TAG, e.getMessage() + "");
                }
            } else {
                Log.e(TAG, "Error connecting");
            }
        }
    };

    public void quitSession() {
        if (mIvcsSocket == null)
            return;
        mIvcsSocket.emit(QUIT, null, args -> {
            if (isConnected) {
                String res = (String) args[0];
                if (res.equals(STATUS_OK)) {
                    MessageData messageData = new MessageData();
                    messageData.setType(CcsMessage.CCS_QUIT_SESSION);
                    messageData.setData(true);
                    mCcsEventMessage.onCcsEvent(messageData);
                } else {
                    MessageData messageData = new MessageData();
                    messageData.setType(CcsMessage.CCS_QUIT_SESSION);
                    messageData.setData(false);
                    mCcsEventMessage.onCcsEvent(messageData);
                }
            }
        });
    }

    /**
     * 发送所有消息，结果由messageType区分
     */
    public void sendMessage(JSONObject message, String messageType) {

        Log.i(TAG, "send message content =" + message.toString());
        mIvcsSocket.emit(SESSION_MESSAGE, message, new Ack() {
            @Override
            public void call(Object... args) {
                if (isConnected) {
                    String res = (String) args[0];
                    if (res.equals(STATUS_OK)) {
                        // TODO 发送消息成功后应该带上 原信息 和 响应信息
                        MessageData messageData = new MessageData();
//                        messageData.setData(messageType);
//                        messageData.setType(CcsMessage.CCS_SUCCEED_TO_SEND_MESSAGE);
                        messageData.setData(message);
                        switch (messageType){
                            case CcsMessage.CCS_TEXT:
                            case CcsMessage.CCS_LINK:
                                // 不需要对信息特别处理
                                messageData.setType(CcsMessage.CCS_SEND_MESSAGE_SUCCESS);
                                break;
                            default:{
                                messageData.setType(messageType);
                            }
                        }
                        mCcsEventMessage.onCcsEvent(messageData);
                    } else {
                        String resp = (String) args[1];
                        MessageData messageData = new MessageData();
                        messageData.setData(resp);
                        messageData.setType(CcsMessage.CCS_FAILED_TO_SEND_MESSAGE);
//                        messageData.setData(message);
//                        messageData.setType(messageType);
                        mCcsEventMessage.onCcsEvent(messageData);
                    }
                }
            }
        });
    }

    public void invite(JSONObject message) {
        mIvcsSocket.emit(CcsMessage.CCS_INVITE, message, new Ack() {
            @Override
            public void call(Object... args) {
                if (isConnected) {
                    String res = (String) args[0];
                    if (res.equals(STATUS_OK)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(CcsMessage.CCS_INVITE_SUCCESS);
                        mCcsEventMessage.onCcsEvent(messageData);
                    } else {
                        String resp = (String) args[1];
                        MessageData messageData = new MessageData();
                        messageData.setData(resp);
                        messageData.setType(CcsMessage.CCS_INVITE_FAILED);
                        mCcsEventMessage.onCcsEvent(messageData);
                    }
                }
            }
        });
    }

    public void transfer(JSONObject message){
        mIvcsSocket.emit(CcsMessage.CCS_INVITE, message, new Ack() {
            @Override
            public void call(Object... args) {
                if (isConnected) {
                    String res = (String) args[0];
                    if (res.equals(STATUS_OK)) {
                        MessageData messageData = new MessageData();
                        messageData.setType(CcsMessage.CCS_INVITE_SUCCESS);
                        mCcsEventMessage.onCcsEvent(messageData);
                    } else {
                        String resp = (String) args[1];
                        MessageData messageData = new MessageData();
                        messageData.setData(resp);
                        messageData.setType(CcsMessage.CCS_INVITE_FAILED);
                        mCcsEventMessage.onCcsEvent(messageData);
                    }
                }
            }
        });
    }

    public interface CcsEventMessage {
        void onCcsEvent(MessageData messageData);
    }
}
