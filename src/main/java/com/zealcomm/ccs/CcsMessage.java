package com.zealcomm.ccs;

public class CcsMessage {

    public static final String CCS_LOGIN_SUCCESS = "ccs_login_success";
    public static final String CCS_LOGIN_ERROR = "ccs_login_error";
    public static final String CCS_CONNECT_SUCCESS = "ccs_connect_success";
    public static final String CCS_CONNECT_ERROR = "ccs_connect_error";
    public static final String CCS_DISCONNECT = "ccs_disconnect";
    public static final String CCS_PEER_STATUS = "peer-status";
    public static final String CCS_READY_TO_TALK = "ready-to-talk";
    public static final String CCS_MESSAGE_CONFIRMATION = "message-confirmation";
    public static final String CCS_HOLD_ON = "hold-on";
    public static final String CCS_QUIT = "quit";
    public static final String CCS_FORM = "form";
    public static final String CCS_COLLECT_INFO = "collect-info";
    public static final String CCS_LINK = "link";
    public static final String CCS_SEND_LINK_SUCCESS = "send-link" ;
    public static final String CCS_TEXT = "text";
    public static final String CCS_HANDWRITING = "handwriting";
    public static final String CCS_PEER_CMD = "peer-cmd";
    public static final String CCS_SCREEN_SHOT = "screen-snapshot";
    public static final String PHOTO_GUIDEBOX = "photo_guidebox";
    public static final String CCS_SNAPSHOT = "take-photo";
    public static final String CCS_DROP = "ccs_drop";
    public static final String CCS_SUCCEED_TO_SEND_MESSAGE = "ccs_succeed_to_send_msg";
    public static final String CCS_FAILED_TO_SEND_MESSAGE = "ccs_failed_to_send_msg";
    public static final String CCS_HANDSIGN = "please handsign";
    public static final String CCS_QUIT_SESSION = "ccs_quit_session";
    public static final String CCS_INVITE = "invite";
    public static final String CCS_INVITE_SUCCESS = "invite_success";
    public static final String CCS_INVITE_FAILED = "invite_failed";
    public static final String LINK_PAGE= "page";
    public static final String USER_JOINED = "joined";
    public static final String USER_QUIT = "quit";
    public static final String CCS_CUSTOM_MESSAGE = "ccs_custom_message";
    public static final String CCS_SEND_MESSAGE_SUCCESS = "ccs_send_message_success";
    public static final String CCS_INVITATION_PROGRESS_MESSAGE = "invitation-progress";
    public static final String CCS_VIEW_SCREEN = "view-screen";
    public static final String CCS_RELEASE_SCREEN = "release-screen";
    public static final String CCS_MARK_SCREEN = "markScreen";
    public static final String CCS_SCREEN_SIZE = "screenSize";
}
